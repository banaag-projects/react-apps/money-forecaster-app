import { configureStore } from "@reduxjs/toolkit";
import cashInSlice from "./reducers/cashIn-slice";
import cashOutSlice from "./reducers/cashOut-slice";
import totalSlice from "./reducers/total-slice";

const store = configureStore({
  reducer: {
    cashInReducer: cashInSlice.reducer,
    cashOutReducer: cashOutSlice.reducer,
    totalReducer: totalSlice.reducer,
  },
});

export default store;
