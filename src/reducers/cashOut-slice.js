import { createSlice } from "@reduxjs/toolkit";

const cashOutSlice = createSlice({
  name: "cashOut",
  initialState: {
    cashOutArray: [], //just for initialization of the array of cash ins
    totalCashOutState: 0,
    idState: 1,
  },
  reducers: {
    addCashOut(state, action) {
      const newCashOut = action.payload;
      const cashOutArrayState = state.cashOutArray;
      state.totalCashOutState += newCashOut.cashOutNumber;

      state.cashOutArray.push({
        cashOutInput: newCashOut.cashOutText,
        cashOutAmount: newCashOut.cashOutNumber,
        id: state.idState++,
      });
    },
    removeCashOut(state, action) {
      const itemID = action.payload;
      const deleteCashOutNumber = state.cashOutArray.find(
        (num) => num.id == itemID
      );

      state.totalCashOutState -= deleteCashOutNumber.cashOutAmount;
      state.cashOutArray = state.cashOutArray.filter(
        (item) => item.id !== itemID
      );
    },
  },
});

export const cashOutActions = cashOutSlice.actions;

export default cashOutSlice;
