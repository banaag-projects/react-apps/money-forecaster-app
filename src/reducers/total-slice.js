import { createSlice } from "@reduxjs/toolkit";

const totalSlice = createSlice({
  name: "totalSavings",
  initialState: {
    totalSavings: 0,
    dateDifference: 0,
  },
  reducers: {
    setTargetDate(state, action) {
      const targetDateToMonth = action.payload.date;
      const monthCurrent = new Date().getMonth() + 1;
      const targetDate = new Date(targetDateToMonth).getMonth() + 1;

      // console.log(`Current Month: ${monthCurrent}`);
      // console.log(`Target date: ${targetDate}`);

      state.dateDifference = targetDate - monthCurrent;
    },
    calcSavings(state, action) {
      const totalCashIn = action.payload.totalCashInItems;
      const totalCashOut = action.payload.totalCashOutItems;

      const amountDiff = totalCashIn - totalCashOut;
      state.totalSavings = amountDiff * state.dateDifference;
    },
  },
});

export const totalActions = totalSlice.actions;

export default totalSlice;
