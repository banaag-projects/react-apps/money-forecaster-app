import { createSlice } from "@reduxjs/toolkit";

const cashInSlice = createSlice({
  name: "cashIn",
  initialState: {
    cashInArray: [], //just for initialization of the array of cash ins
    totalCashInState: 0,
    idState: 1,
  },
  reducers: {
    addCashIn(state, action) {
      const newCashIn = action.payload;
      const cashInArrayState = state.cashInArray;
      state.totalCashInState += newCashIn.cashInNumber;

      state.cashInArray.push({
        cashInInput: newCashIn.cashInText,
        cashInAmount: newCashIn.cashInNumber,
        id: state.idState++,
      });
    },
    removeCashIn(state, action) {
      const itemID = action.payload;
      const deletecashInNumber = state.cashInArray.find(
        (num) => num.id == itemID
      );

      state.totalCashInState -= deletecashInNumber.cashInAmount;
      state.cashInArray = state.cashInArray.filter(
        (item) => item.id !== itemID
      );
    },
  },
});

export const cashInActions = cashInSlice.actions;

export default cashInSlice;
