import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { TextField } from "@mui/material";
import { cashOutActions } from "../reducers/cashOut-slice";

function NewCashOut() {
  const dispatch = useDispatch();

  const [cashOutText, setCashOutText] = useState("");
  const [cashOutNumber, setCashOutNumber] = useState(0);
  const [id, setId] = useState(1);

  const addCashOut = () => {
    dispatch(
      cashOutActions.addCashOut({
        cashOutText,
        cashOutNumber,
        id,
      })
    );
  };

  const convertToInt = (e) => {
    const inputToInt = parseInt(e.target.value);
    setCashOutNumber(inputToInt);
  };

  return (
    <div className="cashOutBox">
      <div className="cashOutInputDiv">
        <div className="cashOutItemLabel" style={{ marginRight: "20px" }}>
          <TextField
            label="Cash-Out Item"
            variant="filled"
            value={cashOutText}
            onChange={(e) => setCashOutText(e.target.value)}
          />
        </div>
        <div className="cashOutItemAmount">
          <TextField
            type="number"
            label="Amount"
            variant="filled"
            placeholder=" "
            value={cashOutNumber}
            onChange={convertToInt}
          />
        </div>
      </div>
      <div>
        <button
          onClick={() => {
            addCashOut();
          }}
        >
          Add Cash Out
        </button>
      </div>
    </div>
  );
}

export default NewCashOut;
