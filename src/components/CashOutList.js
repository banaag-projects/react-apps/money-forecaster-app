import React from "react";
import { useSelector, useDispatch } from "react-redux";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import { Container, Row, Col } from "react-bootstrap";
import { cashOutActions } from "../reducers/cashOut-slice";

export default function CashOutList({ id }) {
  const dispatch = useDispatch();
  const cashOutItems = useSelector(
    (state) => state.cashOutReducer.cashOutArray
  );
  const totalCashOutItems = useSelector(
    (state) => state.cashOutReducer.totalCashOutState
  );

  const removeCashOut = (id) => {
    dispatch(cashOutActions.removeCashOut(id));
  };

  return (
    <Container className="list">
      {cashOutItems.map((element) => {
        return (
          <Container key={element.id}>
            <Row style={{ marginLeft: "5%", marginTop: "10px" }}>
              <Col>{element.cashOutInput}</Col>
              <Col>{element.cashOutAmount}</Col>

              <Col>
                <button onClick={() => removeCashOut(element.id)}>
                  Delete
                </button>

                {/* {cashOutItems.length == 0 ? null : <button>Edit</button>} */}
              </Col>
            </Row>
          </Container>
        );
      })}
    </Container>
  );
}
