import React from "react";
import { useSelector, useDispatch } from "react-redux";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import { Container, Row, Col } from "react-bootstrap";
import { cashInActions } from "../reducers/cashIn-slice";

export default function CashInList({ id }) {
  const dispatch = useDispatch();
  const cashInItems = useSelector((state) => state.cashInReducer.cashInArray);
  const totalCashInItems = useSelector(
    (state) => state.cashInReducer.totalCashInState
  );

  const removeCashIn = (id) => {
    dispatch(cashInActions.removeCashIn(id));
  };

  return (
    <Container className="list">
      {cashInItems.map((element) => {
        // console.log(`Id: ${element.id}`);
        return (
          <Container key={element.id}>
            <Row style={{ marginLeft: "5%", marginTop: "10px" }}>
              <Col>{element.cashInInput}</Col>
              <Col>{element.cashInAmount}</Col>

              <Col>
                <button onClick={() => removeCashIn(element.id)}>Delete</button>
                {/* {cashInItems.length == 0 ? null : <button>Edit</button>} */}
              </Col>
            </Row>
          </Container>
        );
      })}

      {cashInItems !== [null] ? null : <button>Edit</button>}
    </Container>
  );
}
