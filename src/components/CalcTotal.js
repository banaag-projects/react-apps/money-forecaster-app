import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Container, Row, Col } from "react-bootstrap";
import { totalActions } from "../reducers/total-slice";

export default function CalcTotal() {
  const dateDiff = useSelector((state) => state.totalReducer.dateDifference);
  const savings = useSelector((state) => state.totalReducer.totalSavings);
  const totalCashInItems = useSelector(
    (state) => state.cashInReducer.totalCashInState
  );
  const totalCashOutItems = useSelector(
    (state) => state.cashOutReducer.totalCashOutState
  );
  const dispatch = useDispatch();
  const [date, setDate] = useState("");

  useEffect(() => {
    dispatch(totalActions.setTargetDate({ date }));
  }, [date]);

  const calcSavings = () => {
    dispatch(totalActions.calcSavings({ totalCashInItems, totalCashOutItems }));
  };

  return (
    <Container fluid className="calcBox">
      <Row style={{ marginLeft: "10px" }}>
        <Col>
          <h4>Set target date: </h4>
          <input
            type="date"
            value={date}
            onChange={(e) => {
              setDate(e.target.value);
            }}
            style={{ marginTop: "10px", width: "100%" }}
          />

          <button
            style={{ marginTop: "20px", width: "100%" }}
            onClick={calcSavings}
          >
            Calculate Savings
          </button>
        </Col>

        <Col
          style={{
            width: "100%",
            textAlign: "center",
            margin: "10px",
          }}
        >
          <h5>Total Income: ₱{totalCashInItems}</h5>
          <h5>Total Expenses: ₱{totalCashOutItems}</h5>
          <h5 style={{ marginTop: "20px" }}>Savings: ₱{savings}</h5>
        </Col>
      </Row>
    </Container>
  );
}
