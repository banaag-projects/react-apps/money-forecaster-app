import React from "react";
import "./Main.css";
import NewCashIn from "./NewCashIn";
import CashInList from "./CashInList";
import NewCashOut from "./NewCashOut";
import CashOutList from "./CashOutList";
import CalcTotal from "./CalcTotal";
import { Row, Col, Container } from "react-bootstrap";

function Main() {
  return (
    <Container fluid style={{ padding: "20px" }}>
      <Row>
        <Col>
          <NewCashIn />
          <CashInList />
        </Col>
        <Col>
          <NewCashOut />
          <CashOutList />
        </Col>
      </Row>
      <Row>
        <Col>
          <CalcTotal />
        </Col>
      </Row>
    </Container>
  );
}

export default Main;
