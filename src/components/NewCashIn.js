import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { TextField, FormControl } from "@mui/material";
import { cashInActions } from "../reducers/cashIn-slice";
import { Row, Col } from "react-bootstrap";

function NewCashIn() {
  const dispatch = useDispatch();

  const [cashInText, setCashInText] = useState("");
  const [cashInNumber, setCashInNumber] = useState(0);
  const [id, setId] = useState(1);

  const totalCashInItems = useSelector(
    (state) => state.cashInReducer.totalCashInState
  );
  // console.log(`Total cashin reducer: ${totalCashInItems}`);

  const addCashIn = () => {
    dispatch(
      cashInActions.addCashIn({
        cashInText,
        cashInNumber,
        id,
      })
    );
  };

  const convertToInt = (e) => {
    const inputToInt = parseInt(e.target.value);
    setCashInNumber(inputToInt);
  };

  return (
    <div className="cashInBox">
      <div className="cashInInputDiv">
        <FormControl sx={{ mr: 1 }}>
          <TextField
            label="Cash-In Item"
            variant="filled"
            value={cashInText}
            onChange={(e) => setCashInText(e.target.value)}
          />
        </FormControl>
        <FormControl>
          <TextField
            type="number"
            label="Amount"
            variant="filled"
            placeholder=" "
            value={cashInNumber}
            onChange={convertToInt}
          />
        </FormControl>
      </div>
      <div>
        <button
          onClick={() => {
            addCashIn();
          }}
        >
          Add Cash In
        </button>
      </div>
    </div>
  );
}

export default NewCashIn;
