import { CREATE_CASHIN } from "./actions";

const initialState = {
  cashInInput: "",
  cashInValue: 0,
};

export const cashIn = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_TODO: {
      const { cashInInput, cashInValue } = payload;
      const newCashIn = {
        cashInInput,
        cashInValue,
      };
      return state.concat(newCashIn);
    }

    default:
      return state;
  }
};
