import Main from "./components/Main";
import React from "react";
import { Container } from "react-bootstrap";
function App() {
  return (
    <div>
      <Main />
    </div>
  );
}

export default App;
